from django.shortcuts import render
from django.views.generic import ListView,DetailView
from django.views.decorators.csrf import csrf_exempt
 

from .models import *

# Create your views here.
class listing_list(ListView ):
	queryset = listing.objects.all()
	model = listing
	template_name='property.html'

class listing_detail(DetailView):
    model = listing
    template_name = 'property_detail.html'