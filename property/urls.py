from . import views
from django.urls import path
from .views import *


urlpatterns = [
path('', listing_list.as_view(), name='listing_list'),
path('<slug:slug>/',listing_detail.as_view(), name='listing_detail'),   
]