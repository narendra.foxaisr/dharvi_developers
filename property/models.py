from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from datetime import datetime
from django.conf import settings
from django.utils import timezone



	 
class listing(models.Model):
    title = models.CharField(max_length=200)
    slug =models.SlugField(max_length=200)
    address = models.CharField(max_length=200)
    city = models.CharField(max_length=100)
    state = models.CharField(max_length=100)
    zipcode = models.CharField(max_length=20)
    price = models.IntegerField()
    bedrooms = models.IntegerField()
    bathrooms = models.DecimalField(max_digits=2, decimal_places=1)
    garage = models.IntegerField(default=0)
    sqft = models.IntegerField()
    lot_size = models.DecimalField(max_digits=5, decimal_places=1)
    photo_main = models.ImageField(upload_to='property/%m/%d/')
    photo_1 = models.ImageField(upload_to='property/%m/%d/', blank=True)
    photo_2 = models.ImageField(upload_to='property/%m/%d/', blank=True)
    photo_3 = models.ImageField(upload_to='property/%m/%d/', blank=True)
    photo_3 = models.ImageField(upload_to='property/%m/%d/', blank=True)
    photo_4 = models.ImageField(upload_to='property/%m/%d/', blank=True)
    photo_5 = models.ImageField(upload_to='property/%m/%d/', blank=True)
    photo_6 = models.ImageField(upload_to='property/%m/%d/', blank=True)
    description = models.TextField(blank=True)

    is_published = models.BooleanField(default=True)
    list_date = models.DateTimeField(default=datetime.now, blank=True)
   
    def __str__(self):
        return f'{self.title}listing'

    def get_absolute_url(self):
        return reverse('home',kwargs={})	







		


# Create your models here.
