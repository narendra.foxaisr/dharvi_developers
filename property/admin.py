from django.contrib import admin
from .models import *
# Register your models here.
class LocalAdmin(admin.ModelAdmin):
    list_display = ('title', 'city', 'state',  'photo_main',)
    search_fields = ['title', 'city']
    # prepopulated_fields = {'slug': ('title',)}
admin.site.register(listing,LocalAdmin)