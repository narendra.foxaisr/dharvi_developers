from django.db import models
from django.utils import timezone
from django.conf import settings


STATUS=(
		(0,"Drafts"),
    	(1 ,"Publish")
	)

class Post(models.Model):
	title=models.CharField(max_length=200)
	slug =models.SlugField(max_length=200)
	author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
	content=models.TextField()
	created_on =models.DateTimeField(default=timezone.now)
	status=models.IntegerField(choices=STATUS,default=0)
		


class Meta:
        ordering = ['-created_on']

def __str__(self):
	return self.title