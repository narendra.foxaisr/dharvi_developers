from django.views.generic.base import TemplateView
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

# from django

urlpatterns = [
                  path('', TemplateView.as_view(template_name='index.html'), name='index'),
                  path('accounts/', include('account.urls')),
                  path('login/', include('student_task.urls')),
                  path('blog/', include('blog.urls')),
                  path('property/', include('property.urls')),
                  path('about/', TemplateView.as_view(template_name='#'), name='about'),
                  path('contact/', TemplateView.as_view(template_name='contact.html'), name='contact'),
                  path('admin/', admin.site.urls),
                  path('api/', include('api_crud.urls')),
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
