# Generated by Django 3.0.4 on 2020-03-30 09:57

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('student_task', '0003_listing'),
    ]

    operations = [
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200)),
                ('slug', models.SlugField(max_length=200)),
                ('content', models.TextField()),
                ('created_on', models.DateTimeField(default=datetime.datetime.now)),
                ('status', models.IntegerField(choices=[(0, 'Drafts'), (1, 'Publish')], default=0)),
                ('photos', models.ImageField(blank=True, upload_to='blog_images/')),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.DeleteModel(
            name='student',
        ),
    ]
