
from django.contrib import admin
from django.urls import path
from django.conf.urls import url
from . import views
from .views import *
from .views import update_view ,delete_view 
  

# from django.conf import settings
from django.conf.urls.static import static 

urlpatterns = [
   
    path('', views.login, name='login'),   
    path('logout', views.logout, name='logout'),
    
    path('task/',views.home,name='home'),
	path('admin/', admin.site.urls),   
    path('data/new/',DataCreateView.as_view() ,name='data-create'),
    path('task/data/<str:pk>/update/',DataUpdateView.as_view() ,name='data-update'),
    path('task/data/<int:pk>/delete/',DataDeleteView.as_view() ,name='data-delete'),  
  
    path('blog_table/',views.blog_home ,name='blog_home'),
    path('blog_insert/',views.create_view,name='create_view'), 
    path('blog_table/blog/<id>/update/', views.update_view, name='update_view' ), 
    path('blog_table/blog/<id>/delete/',views.delete_view ,name='delete_view'),
]


