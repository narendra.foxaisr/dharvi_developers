from django.shortcuts import (get_object_or_404,render,HttpResponseRedirect) 
from django.shortcuts import render, redirect
from django.http import HttpResponse,HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib import messages, auth
from django.contrib.auth import authenticate
from django.views.generic import ListView,CreateView, UpdateView, DeleteView,DetailView
import pickle
import json
from property.models import listing 
# from proeperty.forms import *
from blog.models import Post
from blog.forms import *
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt

def login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = auth.authenticate(username=username, password=password)

        if user is not None:
            auth.login(request, user)
            messages.success(request, 'You are now logged in')
            return redirect('home')
        else:
            messages.error(request, 'Invalid credentials')
            return redirect('login')    
    else:
        return render(request, 'login.html')
    

def logout(request):
    if request.method == 'POST':
        auth.logout(request)
        messages.success(request, 'You are now logged out')
    return HttpResponseRedirect("/") 
    










 
###for admmin View#
@login_required
def home(request):
	st=listing.objects.all()
	# return HttpResponse(st)
	return render(request=request, template_name="data_table.html", context={"st":st})


class DataCreateView(CreateView):
	model = listing
	fields = '__all__'
	template_name="data_insert.html"

	def form_valid(self, form):
		return super().form_valid(form)

class DataUpdateView(UpdateView ):
	model = listing
	fields = '__all__'
	template_name="data_update.html"
	def form_valid(self, form):
		return super().form_valid(form)


class DataDeleteView(DeleteView):
	model=listing
	success_url="home/"
	template_name = "listing_confirm_delete.html"


# def about(request):
# 	return render(request=request,template_name="#", context={})
# def contact(request):
# 	return render(request=request,template_name="contact.html", context={})


def blog_home(request):
	bt=Post.objects.all()
	# return HttpResponse(st)
	return render(request=request, template_name="blog_table.html", context={"bt":bt})


def create_view(request): 
	
	context ={} 
	form = Postform(request.POST or None) 
	if form.is_valid(): 
		form.save() 
		return HttpResponseRedirect("/login/blog_table/")
	context['form']= form 
	return render(request, "blog_insert.html", context) 


    
# 
def update_view(request, id):      
    context ={}    
    obj = get_object_or_404(Post, id = id)                           # pass the object as instance in form   
    form = Postform(request.POST or None, instance = obj)     
    if form.is_valid(): 
        form.save()                                        
        return HttpResponseRedirect("/login/blog_table/")               #+id
    context["form"] = form   
    return render(request, "blog_update.html", context) 


def delete_view(request, id):     
    context ={} 
    obj = get_object_or_404(Post, id = id)
    if request.method =="POST": 
        obj.delete()
        return HttpResponseRedirect("/login/blog_table/") 
  
    return render(request, "blog_delete.html", context) 



##propeprty list for UI ######











