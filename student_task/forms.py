from django import forms 
from .models import listing 


# creating a form 
class listingform(forms.ModelForm): 

	# create meta class 
	class Meta: 
		# specify model to be used 
		model = listing 

		# specify fields to be used 
		fields = '__all__' 
