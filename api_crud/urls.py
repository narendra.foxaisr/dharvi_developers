from django.urls import path
from . import views


urlpatterns = [
    path('blog_api/',views.blog_list,name='blog_list'),
    path('blog_api/<int:pk>/', views.blog_detail,name='blog_detail'),
    path('property_api/',views.property_list,name='property_list'),
    path('property_api/<int:pk>/', views.property_detail,name='property_detail'),
    
] 
    