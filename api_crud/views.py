from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from django.shortcuts import render
from rest_framework import status
import traceback
import json
from blog.serializers import PostSerializer
from property.serializers import listingSerializer
from rest_framework.decorators import api_view
from rest_framework.response import Response
from blog.models import *
from property.models import *



@api_view (['GET','POST'])
def blog_list(request):
    if request.method == 'GET':
        obj =Post.objects.all()
        serializer = PostSerializer(obj, many=True)
        return Response(serializer.data)

    elif request.method =='POST':
        serializer = PostSerializer(data=request.data) #data ko insert krne ki request deta he
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data ,status=201)
        return Response(serializer.errors ,status=400)

# Create your views here.
@api_view(['GET','PUT','DELETE'])
def blog_detail(request, pk):
    
    try:
        obj= Post.objects.get(pk=pk)
    except Post.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = PostSerializer(obj)
        return Response(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = PostSerializer(obj, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        obj.delete()
        return HttpResponse(status=204)



    #####################################################################################################################
   ## property.api_view

@api_view (['GET','POST'])
def property_list(request):
    if request.method == 'GET':
        obj =listing.objects.all()
        serializer = listingSerializer(obj, many=True)
        return Response(serializer.data)

    elif request.method =='POST':
        serializer = listingSerializer(data=request.data) #data ko insert krne ki request deta he
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data ,status=201)
        return Response(serializer.errors ,status=400)

# Create your views here.
@api_view(['GET','PUT','DELETE'])
def property_detail(request, pk):
    
    try:
        obj= listing.objects.get(pk=pk)
    except listing.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = listingSerializer(obj)
        return Response(serializer.data)

    elif request.method == 'PUT':
        # data = JSONParser().parse(request)
        serializer = listingSerializer(obj, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        obj.delete()
        return HttpResponse(status=204)
