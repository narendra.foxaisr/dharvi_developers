Django==2.1.4
djongo==1.2.32
Pillow==5.1.0
pymongo==3.7.2
six==1.12.0
djangorestframework
